export interface clients{
    id?:string,
    firstname:string,
    lastname:string,    
    email:string,
    phone:string,
    state:number,   
}