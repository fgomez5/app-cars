import { IonToast, IonInput, useIonViewWillEnter, IonIcon, IonCardContent, IonCardTitle, IonButton, IonButtons, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar, IonList, IonItem, IonLabel, IonCard, IonCardHeader } from '@ionic/react';
import { addOutline, trashBinOutline, pencil } from 'ionicons/icons';
import { useParams } from 'react-router';
import ExploreContainer from '../components/ExploreContainer';
import './Report.css';
import { firebaseConfig } from '../database/config'
import firebase from 'firebase/app';
import 'firebase/firebase-firestore';
import { clients } from '../model/clients'
import React, { useEffect, useState } from 'react';
import FusionCharts from "fusioncharts";
import Charts from "fusioncharts/fusioncharts.charts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import ReactFC from "react-fusioncharts";
ReactFC.fcRoot(FusionCharts, Charts, FusionTheme);

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const Report: React.FC = () => {


  const [listclients, setlistclients] = useState<clients[]>([]);
  const [id, setId] = useState('');
  const [firstname, setfirstname] = useState('');
  const [lastname, setlastname] = useState('');
  const [state, setstate] = useState('');
  const [phone, setphone] = useState('');
  const [email, setemail] = useState('');
  const [flag, setflag] = useState(true);
  const [messaje, setmessaje] = useState(false);
  const [dataGraph, setdataGraph] = useState<{ label: string; value: number; }[]>([]);
  const [datefind, setdatefind] = useState();

  function GetDataFinFirebase(datefind: number) {
    var datefind22 = new Date()
    var datefind2 = new Date(datefind22.setDate(datefind22.getDate() - datefind));
    var date1 = new Date(datefind2.setDate(datefind2.getDate() - 1));
    var date2 = new Date(datefind2.setDate(datefind2.getDate() + 1));
    firebase.firestore()
      .collection('clients')
      .orderBy('createdAt')
      .where('createdAt', '>', new Date(2021, 8, 16))
      .where('createdAt', '<', new Date(2021, 8, 18))
      .get().then(({ docs }) => {
        console.log(docs.length);
        return docs.length
      });

  }

  var date11 = new Date();
  var date111 = new Date();
  var date22 = new Date();
  var date222 = new Date();
  var date33 = new Date();
  var date333 = new Date();
  var date44 = new Date();
  var date444 = new Date();
  var date55 = new Date();
  var date555 = new Date();
  var date66 = new Date();
  var date666 = new Date();
  var date77 = new Date();
  var date777 = new Date();
  var date88 = new Date();
  var date888 = new Date();
  var date99 = new Date();
  var date999 = new Date();
  var date10 = new Date();
  var date1010 = new Date();
  const dataSource = {
    chart: {
      caption: "Cantidad de Clientes",
      subCaption: "Creados en los ultimos 7 días",
      xAxisName: "Fecha",
      yAxisName: "Cantidad",

      theme: "fusion"
    },
    data: [
      { label: date11.getDate() + "-" + (date11.getMonth() + 1) + "-" + date11.getFullYear(), value: GetDataFinFirebase(0) },
      { label: new Date(date22.setDate(date222.getDate() - 1)).getDate() + "-" + (new Date(date22).getMonth() + 1) + "-" + new Date(date22).getFullYear(), value: GetDataFinFirebase(1) },
      { label: new Date(date33.setDate(date333.getDate() - 2)).getDate() + "-" + (new Date(date33).getMonth() + 1) + "-" + new Date(date33).getFullYear(), value: GetDataFinFirebase(2) },
      { label: new Date(date44.setDate(date444.getDate() - 3)).getDate() + "-" + (new Date(date44).getMonth() + 1) + "-" + new Date(date44).getFullYear(), value: GetDataFinFirebase(3) },
      { label: new Date(date55.setDate(date555.getDate() - 4)).getDate() + "-" + (new Date(date55).getMonth() + 1) + "-" + new Date(date55).getFullYear(), value: GetDataFinFirebase(4) },
      { label: new Date(date66.setDate(date666.getDate() - 5)).getDate() + "-" + (new Date(date66).getMonth() + 1) + "-" + new Date(date66).getFullYear(), value: GetDataFinFirebase(5) },
      { label: new Date(date77.setDate(date777.getDate() - 6)).getDate() + "-" + (new Date(date77).getMonth() + 1) + "-" + new Date(date77).getFullYear(), value: GetDataFinFirebase(6) },
    ]
  };
  const chartConfigs = {
    type: "column2d",
    width: 600,
    height: 400,
    dataFormat: "json",
    dataSource: dataSource
  };

  const { name } = useParams<{ name: string; values: "Módulo Reporte" }>();

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton></IonMenuButton>
          </IonButtons>
          <IonTitle>Módulo de Reportes</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <ReactFC {...chartConfigs} />
      </IonContent>
    </IonPage>
  );
};

export default Report;
