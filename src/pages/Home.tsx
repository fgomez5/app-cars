import { IonCardSubtitle, IonCardContent, IonCardTitle,  IonButtons, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar, IonList, IonItem, IonLabel, IonCard, IonCardHeader } from '@ionic/react';
import { addOutline, trashBinOutline, pencil, ellipsisHorizontal, ellipsisVertical } from 'ionicons/icons';
import { useParams } from 'react-router';
import ExploreContainer from '../components/ExploreContainer';
import './Home.css';
import React, { useState } from 'react';


const Home: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton></IonMenuButton>
          </IonButtons>
          <IonTitle>APP CARS FER</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonCard>
          <IonCardHeader>            
          </IonCardHeader>
          <IonCardContent>
            Bienvenido al Sistema de APP CARS FER            
          </IonCardContent>
          <IonCardContent>            
            Para navegar en el presionar el menú en la parte superior izquierda para empezar a utilizarlo
          </IonCardContent>
        </IonCard>
      </IonContent>
    </IonPage>
  );
};

export default Home;
