import { IonToast, IonInput, useIonViewWillEnter, IonIcon, IonCardContent, IonCardTitle, IonButton, IonButtons, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar, IonList, IonItem, IonLabel, IonCard, IonCardHeader } from '@ionic/react';
import { addOutline, trashBinOutline, pencil, ellipsisHorizontal, ellipsisVertical } from 'ionicons/icons';
import { useParams } from 'react-router';
import ExploreContainer from '../components/ExploreContainer';
import './Clientes.css';
import { firebaseConfig } from '../database/config'
import firebase from 'firebase/app';
import 'firebase/firebase-firestore';
import { clients } from '../model/clients'
import React, { useState } from 'react';

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const Clientes: React.FC = () => {


  const [listclients, setlistclients] = useState<clients[]>([]);
  const [id, setId] = useState('');
  const [firstname, setfirstname] = useState('');
  const [lastname, setlastname] = useState('');
  const [state, setstate] = useState('');
  const [phone, setphone] = useState('');
  const [email, setemail] = useState(''); 
  const [flag, setflag] = useState(true);
  const [messaje, setmessaje] = useState(false);

  const listar = async () => {
    try {
      let list: clients[] = []
      const res = await firebase.firestore().collection('clients').get();

      res.forEach((doc) => {
        let obj = {
          id: doc.id,
          firstname: doc.data().firstname,
          lastname: doc.data().lastname,
          email: doc.data().email,
          phone: doc.data().phone,
          state: doc.data().state,          
        };
        list.push(obj)

      });
      setlistclients(list)

    } catch (error) { }
  }

  const create = async () => {
    try {      
      if (flag) {
        await firebase.firestore().collection('clients').add(
          { firstname, lastname, email, phone, state:1, createdAt: firebase.firestore.Timestamp.fromDate(new Date())});

      } else {
        await firebase.firestore().collection('clients').doc(id).set(
          { firstname, lastname, email, phone, state });
        setflag(true);
      }

    } catch (error) { }
    setId('');
    setfirstname('');
    setlastname('');
    setphone('');
    setemail('');
    listar();
  }


  const deleted = async (id: string) => {
    try {
      console.log(id)
      await firebase.firestore().collection('clients').doc(id).delete();
      listar();
    } catch (error) { }
  }

  const edit = (id: string, firstname: string, lastname: string, phone: string, email: string) => {
    setId(id);
    setfirstname(firstname);
    setlastname(lastname);
    setphone(phone);
    setemail(email);
    setflag(false);
  }

  useIonViewWillEnter(() => {
    listar();
  })

  const { name } = useParams<{ name: string; values: "Módulo Clientes" }>();

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton></IonMenuButton>            
          </IonButtons>
          <IonTitle>Módulo de Clientes</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonToast
        isOpen={messaje}
        onDidDismiss={() => setmessaje(false)}
        message="Cliente guardado"
        duration={500}
      />
      <IonContent>
        <IonCard>
          <IonItem>
            <IonInput value={firstname}
              placeholder="Nombre"
              onIonChange={e => setfirstname(e.detail.value!)}
            ></IonInput>
          </IonItem>
          <IonItem>
            <IonInput value={lastname}
              placeholder="Apellido"
              onIonChange={e => setlastname(e.detail.value!)}
            ></IonInput>
          </IonItem>
          <IonItem>
            <IonInput value={phone}
              placeholder="Teléfono"
              onIonChange={e => setphone(e.detail.value!)}
            ></IonInput>
          </IonItem>
          <IonItem>
            <IonInput value={email}
              placeholder="Correo"
              onIonChange={e => setemail(e.detail.value!)}
            ></IonInput>
          </IonItem>
          <IonButton color="success" expand="block"
            onClick={() => create()}>
            <IonIcon icon={addOutline}>
            </IonIcon>{flag ? 'Clientes' : 'Editar'}</IonButton>
        </IonCard>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Módulo de Clientes</IonTitle>
          </IonToolbar>
        </IonHeader>
        <ExploreContainer name={name} />
        {/*-- List of Text Items --*/}
        <IonList>
          {
            listclients.map(clients => (
              <IonCard key={clients.id} >
                <IonCardHeader>
                  <IonCardTitle>
                    {clients.firstname} {clients.lastname}
                  </IonCardTitle>
                </IonCardHeader>
                <IonCardContent>
                  <IonList>
                    <IonItem>
                      <IonLabel>Teléfono: {clients.phone}</IonLabel>
                    </IonItem>
                    <IonItem>
                      <IonLabel>Email: {clients.email}</IonLabel>
                    </IonItem>
                  </IonList>
                  <IonButton color="danger" expand="block"
                    onClick={() => deleted('' + clients.id)}>
                    <IonIcon icon={trashBinOutline}></IonIcon>
                    Eliminar</IonButton>
                  <IonButton color="tertiary" expand="block"
                    onClick={
                      () => edit('' + clients.id, '' + clients.firstname, '' + clients.lastname, '' + clients.phone, '' + clients.email)}>
                    <IonIcon icon={pencil}></IonIcon>Editar</IonButton>
                </IonCardContent>

              </IonCard>
            ))}
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default Clientes;
