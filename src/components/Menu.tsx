import {
  IonContent,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenu,
  IonMenuToggle,
  IonNote,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonRouterOutlet
} from '@ionic/react';

import { useLocation } from 'react-router-dom';
import { person, homeOutline, barChartOutline } from 'ionicons/icons';
import './Menu.css';



interface AppPage {
  url: string;
  iosIcon: string;
  mdIcon: string;
  title: string;
}

const appPages: AppPage[] = [
  {
    title: 'HOME',
    url: '/page/Home',
    iosIcon: homeOutline,
    mdIcon: homeOutline
  },
  {
    title: 'Clientes',
    url: '/page/Clientes',
    iosIcon: person,
    mdIcon: person
  },
  {
    title: 'Reportes',
    url: '/page/Reportes',
    iosIcon: barChartOutline,
    mdIcon: barChartOutline
  }
];

const Menu: React.FC = () => {
  const location = useLocation();

  return (   
      <IonMenu contentId="content" side="start" menuId="first">
        <IonHeader>
          <IonToolbar color="primary">
            <IonTitle>Menú</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonList>
            {appPages.map((appPage, index) => {
              return (               
                  <IonItem className={location.pathname === appPage.url ? 'selected' : ''} routerLink={appPage.url} routerDirection="none" lines="none" detail={false}>
                    <IonIcon slot="start" ios={appPage.iosIcon} md={appPage.mdIcon} />
                    <IonLabel>{appPage.title}</IonLabel>
                  </IonItem>               
              );
            })}
          </IonList>
        </IonContent>
      </IonMenu>
 
  );
};

export default Menu;
